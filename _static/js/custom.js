$(document).ready(() => {
  // Change indentation from spaces to tabs for codeblocks.
  const codeBlocks = document.querySelectorAll(
    '.rst-content div[class^="highlight"] pre'
  )
  for (const codeBlock of codeBlocks) {
    const classList = codeBlock.parentNode.parentNode.classList
    // Only change indentation for GDScript code blocks
    if (!classList.contains('highlight-gdscript')) {
      continue
    }
    let html = codeBlock.innerHTML.replace(/^(<span class="w">)?( {4})/gm, '\t')
    let html_old = ''
    while (html != html_old) {
      html_old = html
      html = html.replace(/\t( {4})/gm, '\t\t')
    }
    codeBlock.innerHTML = html
  }
})
