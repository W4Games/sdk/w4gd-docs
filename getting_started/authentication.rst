.. _doc_getting_started_authentication:

Player Authentication
=====================

.. warning:: This is currently a placeholder.

    This page will eventually include in-depth reference on how to
    handle player authentication with W4 Cloud.
    For now, refer to the :ref:`Authenticating Players section
    <doc_tutorials_getting_started_authenticating_players>` of the hands-on
    Getting Started tutorial.

.. Outline:
.. - Players need to be identified in the database somehow
.. - Show email sign up and sign in
.. - Show device ID
