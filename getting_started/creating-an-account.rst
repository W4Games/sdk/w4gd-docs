.. _doc_getting_started_creating_an_account:

Creating a W4 Account
---------------------

To use W4 services, you need to request a workspace.

To do that, go to `<https://cloud.w4.gd/signup>`__, and create an account. Once you log in, you'll see a billing dashboard that enables you to request a workspace:

.. image:: img/010.request-a-workspace-dashboard.webp

Fill in the form. Make sure to provide detailed information about your company and the project you're working on, because it will determine whether your request will be approved.

.. note:: 
  On this page you can keep track of the status of your request - whether it is approved, denied, canceled (by you) or still pending.
  
  While you wait for your workspace request to be approved, you can read through these docs, and get started with our :ref:`tutorial <doc_tutorials_getting_started_index>`.

Once your request is approved, you will receive an email enabling you to set up your admin credentials. You may also receive an onboarding email from the W4 support team welcoming you. You will see your new workspace in the worskpaces table:

.. image:: img/011.new-workspace.webp

To open the workspace dashboard, click on your workspace name (``my-workspace-name`` in the screenshot). It will take you to a URL like ``https://my-workspace-name.beta-01.w4games.cloud/``.

The default administrator of the workspace will have the same email as the one you used in the billing dashboard to request the workspace. It's important to note that even though the two accounts share the same email, they are not the same - your workspace admin account is separate from your billing dashboard account.


After creating an admin account, you will land on your workspace dashboard:

.. image:: img/012.workspace-dashboard.webp

This is the main dashboard you will use to configure and monitor your projects.


.. warning::
   Keep in mind this is a beta environment and we might need to terminate your workspace at any time due to the technical constraints. You should not use the services for production workloads. 
   
   If your request includes the gameserver option, keep in mind that during the beta stage, you are limited to up to 5 gameservers. If you feel this might not be enough for your usecase, you can get open an `issue <https://gitlab.com/W4Games/sdk/w4gd-docs/-/issues>`__ or send an email to `support-cloud@w4games.com <mailto:support-cloud@w4games.com>`__.