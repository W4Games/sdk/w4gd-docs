.. _doc_getting_started_database:

Database (Supabase)
===================

.. warning:: This is currently a placeholder.

    This section will eventually include in-depth reference on how to
    manage your database with Supabase and W4 Cloud, including details on
    our W4RM SDK, storage, real-time features, and various examples.

    For now, refer to the :ref:`Database Management section
    <doc_tutorials_getting_started_database_management>` of the hands-on
    Getting Started tutorial.

.. .. toctree::
..    :maxdepth: 1
..
..    w4rm
..    storage
..    realtime
..    examples/index

